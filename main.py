import datetime as dt
from typing import List, Dict

import mysql.connector
from fastapi import FastAPI

app = FastAPI()


@app.get('/')
def hello_world() -> str:
    return 'Hello, world'


@app.get('/greet/all_greetings')
def all_greetings() -> List[Dict[str, str | dt.datetime]]:
    mydb = mysql.connector.connect(
        host='mysqldb',
        user='root',
        password='p@ssw0rd1',
        database='docker_example'
    )
    cursor = mydb.cursor()
    cursor.execute('SELECT * FROM docker_example.greetings')
    row_headers = [x[0] for x in cursor.description]
    results = cursor.fetchall()
    json_data = []
    for result in results:
        json_data.append(dict(zip(row_headers, result)))
    cursor.close()
    return json_data


@app.get('/greet/name/{greet_target}')
def greet(greet_target) -> str:
    mydb = mysql.connector.connect(
        host='mysqldb',
        user='root',
        password='p@ssw0rd1',
        database='docker_example'
    )
    cursor = mydb.cursor()
    cursor.execute(f'INSERT INTO greetings (Name, Timestamp) VALUES ("{greet_target}", NOW())')
    mydb.commit()
    cursor.close()
    return f'Hello, {greet_target}'


@app.get('/initdb')
def db_init() -> str:
    mydb = mysql.connector.connect(
        host='mysqldb',
        user='root',
        password='p@ssw0rd1'
    )
    cursor = mydb.cursor()
    cursor.execute('DROP DATABASE IF EXISTS docker_example')
    cursor.execute('CREATE DATABASE docker_example')
    cursor.execute('USE docker_example')
    cursor.execute('DROP TABLE IF EXISTS greetings')
    cursor.execute('CREATE TABLE greetings (Name VARCHAR(255), Timestamp DATETIME default current_timestamp())')
    cursor.close()
    return 'Database initialised'

